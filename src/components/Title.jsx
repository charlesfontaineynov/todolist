/* eslint-disable */

import React from 'react';

const Title = ({ title }) => {
  return (
    <h1 className="my-title">{ title }</h1>
  );
}

export default Title;
