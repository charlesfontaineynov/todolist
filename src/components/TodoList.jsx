/* eslint-disable */

import React, { useState, useEffect } from 'react';

/* const props = {
  list: [],
}; */

const TodoList = (props) => {

  const [ message, setMessage ] = useState('');
  /* const list = props.list; */
  const { list } = props;


  useEffect(() => {
    console.log('Mounted');

    if(list.length) {
      console.log('Updated');

      setMessage('New message !');

      window.setTimeout(() => {
        setMessage('')
      }, 2000);
    }
  }, [list]);

  return (
    <>
      <p>{ message }</p>
      <ul>
        { list.map((todo) => <li>{ todo }</li>) }
      </ul>
      <p>Hello world !</p>
    </>
  );
}

export default TodoList;