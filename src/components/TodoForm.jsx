/* eslint-disable */
import React, { useState } from 'react';
import TodoList from './TodoList';

const TodoForm = () => {

  const [ value, setValue ] = useState('');
  const [ todos, setTodos ] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(value);
    setValue('');
  }

  const addTodo = () => {
    setTodos([...todos, value]);
    setValue('');
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
          <input
              type="text"
              value={ value }
              onChange={ (e) => setValue(e.target.value) } />

          <button type="submit" onClick={ addTodo }>
            Ajouter
          </button>
      </form>

      {/* <p>{ value }</p> */}

      {/* <ul>
        { todos.map((todo) => <li>{ todo }</li>) }
      </ul> */}

      <TodoList list={ todos } />
    </div>
  );
}

export default TodoForm;