/* eslint-disable */

import React from 'react';
import './App.css';
import logo from './logo.svg';
import Title from './components/Title';
import TodoForm from './components/TodoForm';

const App = () => (
  <div className="App">
    <header className="App-header">
      Mon app
      <Title title="Ma super Application" />
      <TodoForm />
    </header>
  </div>
);

export default App;
